#!/bin/bash
# A basic script with some looping.

COUNTER=0

while [ $COUNTER -le 10 ];
do
	echo "The counter is at $COUNTER"
	((COUNTER++))
done
